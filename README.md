# Juan José González Izquierdo
## Web Developer

# Delivery

### Tech:

 - Vue
    - Vue Router
    - Vuex
 - Boostrap Grid
 - CSS3
    - SCSS
    - Display Grid
    - Display Flex
    - Media queries for Phones, Tablets, Desktop, HD.
 - HTML
    - BEM (Block, Element, Modifier)


### Explanation
If it's your first visit on this page, the app obtains the information from a JSON located in ***src/helpers/famous.js***. After, when You vote for someone, the data is stored in Local Storage, to keep the data.


### Rule of Thumb.

- [Home](http://u19171055.onlinehome-server.com/others/test/#/)
- [Past Trials](http://u19171055.onlinehome-server.com/others/test/#/past-trials/all): This page obtains the JSON elements and then filter according to the selected category.
- [View Full Report](http://u19171055.onlinehome-server.com/others/test/#/famous/1): This section based on the ***id route param***, filters the JSON elements and obtains the requested information
- [How It Works](http://u19171055.onlinehome-server.com/others/test/#/how-it-works)
- [Login](http://u19171055.onlinehome-server.com/others/test/#/login)


### Project setup and Installation
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

Many thanks,
Regards