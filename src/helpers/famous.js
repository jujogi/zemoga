const famous = [
  {
    id: 0,
    name: "Pope Francis",
    description:
      "He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)",
    image: "pope.jpg",
    category: "entertainment",
    up: 500,
    down: 100
  },
    {
      id: 1,
      name: "Kanye West",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim obcaecati cupiditate tempora optio suscipit vitae mol",
      image: "kanye.jpg",
      category: "entertainment",
      up: 10,
      down: 5
    },
    {
      id: 2,
      name: "Mark Zuckerberg",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim obcaecati cupiditate tempora optio suscipit vitae mol",
      image: "mark.jpg",
      category: "business",
      up: 10,
      down: 55
    },
    {
      id: 3,
      name: "Cristina Fernández",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim obcaecati cupiditate tempora optio suscipit vitae mol",
      image: "cristina.jpg",
      category: "politics",
      up: 20,
      down: 80
    },
    {
      id: 4,
      name: "Malala Yousafzai",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim obcaecati cupiditate tempora optio suscipit vitae mol",
      image: "malala.jpg",
      category: "entertainment",
      up: 100,
      down: 10
    }
  ],
  famousLocalStorage = JSON.parse(localStorage.getItem("famous"));

export default famousLocalStorage ? famousLocalStorage : famous;
