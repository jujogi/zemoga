import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../pages/Home.vue";
import Works from "../pages/Works.vue";
import Trials from "../pages/Trials.vue";
import Famous from "../pages/Famous.vue";
import Login from "../pages/Login.vue";


Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        component: Home
    },
    {
        path: "/how-it-works",
        component: Works
    },
    {
        path: "/past-trials/:category",
        name: "Trials",
        component: Trials,
        props: true
    },
    {
        path: "/famous/:id",
        component: Famous,
        props: true
    },
    {
        path: "/login",
        component: Login
    },
];

export default new VueRouter({
     routes,
     scrollBehavior () {
        return { x: 0, y: 0 }
      }
 });

 