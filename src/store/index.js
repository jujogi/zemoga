import Vue from 'vue'
import Vuex from 'vuex'
import Famous from '../helpers/famous.js'

Vue.use(Vuex);

export default new Vuex.Store({

    state: {
        famous: Famous
    },

    getters: {
        getFamous: state => state.famous,
        getFamousById: state => id => {
            return state.famous.find(famous => famous.id === id)
        },
    },

    mutations: {
        ADD_SCORE_TO_FAMOUS: (state, payload) => {

            let famous = state.famous.find(famous => famous.id === payload.id),
            property = payload.sum

            famous[property] ++;

            localStorage.setItem('famous', JSON.stringify(state.famous))


        }
    },

    actions: {
        addScoreToFamous: (context, payload) => {
            context.commit('ADD_SCORE_TO_FAMOUS', payload)
        }
    }

});